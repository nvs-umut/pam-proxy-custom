pam-proxy-service: controller and proxy service for PAM Workflow engine
======================================================
Author: Nevin Zhu  
Level: Intermediate  
Technologies: JBoss PAM 7.x, SpringBoot
Summary: The `pam-proxy-service` is an app that interacts with the workflow engine on JBoss PAM 7.x and facilitate communication between PAM and external systems such as message broker  
Target Product: JBoss PAM 7.x

What is it?
-----------

The `pam-proxy-service` demonstrates how to initiate workflow instances with JBoss PAM 7.x and act as a proxy to cascade messages to external system from PAM

It contains the following:

1. Message consumer service 
2. Message producer service
3. Workflow initiator service 



System requirements
-------------------

The application this project produces is designed to be run on maven 3.x

All you need to build this project is Java 8.0 (Java SDK 1.8) or later, Maven 3.0 or later.

 
Configure Maven
---------------

If you have not yet done so, you must [Configure Maven](p:/Nevin/Raytheon - PAM 7 AWS Installation - v1.0.docx) before running the project.


Build and run the Project on Local Workstation
-----------------------------------------------

_NOTE: The following build command assumes you have configured your Maven user settings. If you have not, you must include Maven setting arguments on the command line. 

1. Open a command prompt and navigate to the root directory of this project.
2. Type this command to build and run the project assuming this is running on your local workstation:

        >mvn clean install
        >java -jar target/pam-proxy-service-1.0.0-SNAPSHOT.jar
        

Build and run the Project on DEV environment
-----------------------------------------------

1. Open a command prompt and navigate to the root directory of this project.
2. Type this command to build and run the project assuming this is running on your local workstation:

        >mvn clean install
        >java -Dspring.profiles.active=dev -jar target/pam-proxy-service-1.0.0-SNAPSHOT.jar
        

Investigate the Console Output
------------------------------------

If the command is successful, you will see output similar to this:
```
----
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.6.RELEASE)

2020-03-09 23:28:19.998  INFO 44013 --- [           main] com.malware.MainApplication              : Starting MainApplication on Nevins-MacBook-Pro.local with PID 44013 (/Users/nzhu/Dev/git/raytheon/pam-controller2/target/pam-proxy-service-1.0.0-SNAPSHOT.jar started by nzhu in /Users/nzhu/Dev/git/raytheon/pam-controller2)
2020-03-09 23:28:20.001  INFO 44013 --- [           main] com.malware.MainApplication              : No active profile set, falling back to default profiles: default
2020-03-09 23:28:21.065  INFO 44013 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8085 (http)
2020-03-09 23:28:21.099  INFO 44013 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2020-03-09 23:28:21.099  INFO 44013 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.21]
2020-03-09 23:28:21.209  INFO 44013 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2020-03-09 23:28:21.209  INFO 44013 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 1157 ms
2020-03-09 23:28:21.504  INFO 44013 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2020-03-09 23:28:22.217  INFO 44013 --- [localhost:5672]] o.a.qpid.jms.sasl.SaslMechanismFinder    : Best match for SASL auth was: SASL-ANONYMOUS
2020-03-09 23:28:22.273  INFO 44013 --- [localhost:5672]] org.apache.qpid.jms.JmsConnection        : Connection ID:ff042793-63d6-4e33-8f56-75cdd4454374:1 connected to remote Broker: amqp://localhost:5672
2020-03-09 23:28:22.297  INFO 44013 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8085 (http) with context path ''
2020-03-09 23:28:22.301  INFO 44013 --- [           main] com.malware.MainApplication              : Started MainApplication in 2.817 seconds (JVM running for 3.297)
```
----

Deploying into Openshift 3.11
------------------------------------
Prerequisites
- Openshift 3.11 Cluster w/ Persistent Storage Enabled
- Access to OCP Web Console
- AMQ Broker 7.6.x (on same namespace)
- RHPAM KIE Server 7.7.x (on same namespace)
- PAM Orchestrator Application deployed on KIE Server

1. `oc login <openshift_cluster_endpoint>`
2. Go into the `oc project <openshift_namespace>`
3. Create a config map of the application.properties file. `oc create configmap app-config --from-file=src/test/resources/application.properties`
4. Deploy pam proxy service application into Openshift using the redhat openjdk imagestream. `oc new-app registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift~https://github.com/nzhu972/pam-proxy-service.git` 
5. Once application is built and deployed, mount the config map to the application. `oc set volume dc/pam-proxy-service --add --type=configmap --configmap-name=app-config --mount-path=/deployments/config`
6. Access the OCP web console. 
7. Edit the config map. Project > Resources > Config Maps > "Name_of_Config_Map" > Actions > Edit
8. The following variables will need to be updated -
- spring.pam.url=http://<myapp-kieserver-projectname.example.com>/services/rest/server
- spring.pam.user=<pam_user>
- spring.pam.password=<pam_password>
- id.generator.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/static/send
- static.analysis.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/static/send
- dynamic.analysis.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/dynamic/send
- submission.record.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/mock/submissions
- move.file.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/mock/move_files
- create.metadata.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/mock/create_metadata
- nsrl.lookup.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/mock/nsrl_lookup
- create.trecord.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/mock/create_trecord
- create.hash.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/mock/create_hash
- unzip.file.url=http://<pam-proxy-service.projectname.svc.cluster.local>:8085/mock/unzip
- amqphub.amqp10jms.remote-url=amqp://<broker-amq-headless.projectname.svc.cluster.local>:5672
9. Save the Config Map.
10. Edit the pam-proxy-service Service. Applications > Services > "pam-proxy-service" > Actions > Edit YAML.
11. Under spec -> ports, add a section for port 8085.
```
- name: 8085-tcp
  port: 8085
  protocol: TCP
  targetPort: 8085

```
12. Save the Service.
13. Rebuild the application.  
   
Troubleshooting
------------------------------------
There are occasions where the dynamic and static queues are not created as anycast on the AMQ broker. Instead, they are created as multicast and no messages are sent the destination. If this happens, run the following command to manually create the queues:

     <broker-instance-dir>/bin/artemis queue create --name <destination-name> --auto-create-address --anycast
